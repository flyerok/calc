#include "Calc.hpp"

Calc::Calc(int x, int y) {
	this->x = x;
	this->y = y;
}

int Calc::sum() {
	return this->x + this->y;
}

int Calc::diff() {
	return this->x - this->y;
}

int Calc::mult() {
	return this->x * this->y;
}

int Calc::div() {
	return this->x / this->y;
}

Calc::~Calc() {}