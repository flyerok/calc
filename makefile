all: main calc
	g++ main.o Calc.o -o main.out

calc:
	g++ -c Calc.cpp -o Calc.o

main:
	g++ -c main.cpp -o main.o
