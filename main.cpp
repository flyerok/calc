#include <iostream>
#include "Calc.hpp"

int main() {
	Calc calc(5, 4);

	std::cout << calc.sum() << std::endl;
	std::cout << calc.diff() << std::endl;
	std::cout << calc.mult() << std::endl;
	std::cout << calc.div() << std::endl;

	return 0;
}