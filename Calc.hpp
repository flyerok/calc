#include <iostream>

class Calc {
private:
	int x;
	int y;

public:
	Calc(int x, int y);

	int sum();
	int diff();
	int mult();
	int div();

	~Calc();
};
